package com.abirpahlwan;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnagramDetectorTest {

    AnagramDetector anagramDetector;

    @BeforeEach
    void init(){
        anagramDetector = new AnagramDetector();
    }

    // ‘bleat’ and ‘table’ are anagrams
    @Test
    void isAnagram() {
        String firstInput = "bleat";
        String secondInput = "table";

        assertTrue(anagramDetector.isAnagram(firstInput, secondInput));
    }

    // ‘eat’ and ‘tar’ are not
    @Test
    void isNotAnagram() {
        String firstInput = "eat";
        String secondInput = "tar";

        assertFalse(anagramDetector.isAnagram(firstInput, secondInput));
    }
}