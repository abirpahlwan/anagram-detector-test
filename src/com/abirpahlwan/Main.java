package com.abirpahlwan;

public class Main {

    public static void main(String[] args) {
        AnagramDetector anagramDetector = new AnagramDetector();
        System.out.println(anagramDetector.isAnagram("car", "table"));
    }
}
