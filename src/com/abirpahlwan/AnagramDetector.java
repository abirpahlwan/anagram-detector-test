package com.abirpahlwan;

import java.util.Arrays;

public class AnagramDetector {

    public boolean isAnagram(String firstString, String secondString){
        char[] firstCharArray = firstString.toLowerCase().toCharArray();
        char[] secondCharArray = secondString.toLowerCase().toCharArray();

        Arrays.sort(firstCharArray);
        Arrays.sort(secondCharArray);

        return Arrays.equals(firstCharArray, secondCharArray);
    }
}
